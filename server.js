// Importing required modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// Initializing express application
const app = express();

// Middleware to handle CORS (Cross-Origin Resource Sharing)
app.use(cors());
// Middleware to parse JSON bodies
app.use(express.json());

// -----------------------MongoDB setup and connection--------------------------
// MongoDB URI (Uniform Resource Identifier)
const uri = "mongodb+srv://ianngoune:Perfect7ENSIIE@cluster0.mo4c1xw.mongodb.net/?retryWrites=true&w=majority";

// Connecting to MongoDB database
mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(async () => {
    console.log('Connected to MongoDB Successfully!!!')
    // try {
    //   const collection = BorrowedItem.collection;
    //   await collection.dropIndex('id_item_borrowed_1'); // Replace with your actual index name
    //   console.log('Unique index on id_item_borrowed removed.');
    // } catch (error) {
    //   console.error('Error removing index:', error.message);
    // }
  }) // Log success message on successful connection
  .catch((err) => console.log(err)); // Log error if connection fails

// Starting the express server
app.listen(3000, () => {
  console.log('Server is running on port 3000');
});
// ----------------------------------------------------------------

//--------------------- RestAPI for retrieving items from DB --------------------
// Importing Item model
const Item = require('./Models/itemModel');

// Endpoint to get all items
app.get('/items', async (req, res) => {
  try {
    // Fetching all items from the database
    const items = await Item.find();
    // Sending the fetched items as response
    res.json(items);
  } catch (err) {
    // Sending error message as response if an error occurs
    res.status(500).send(err.message);
  }
});

// Endpoint to create items
app.post('/items', async (req, res) => {
  try {
    // Creating a new item with the data from request body
    const newItem = new Item(req.body);
    // Saving the new item to the database
    await newItem.save();
    // Sending the newly created item as response
    res.status(201).json(newItem);
  } catch (err) {
    // Sending error message as response if an error occurs
    res.status(500).send(err.message);
  }
});

// Endpoint to get a single item by ID
app.get('/items/:id', async (req, res) => {
  try {
    // Fetching an item by ID from the database
    const item = await Item.findById(req.params.id);
    if (!item) {
      // Sending not found message as response if no item is found
      res.status(404).send('Item not found');
    } else {
      // Sending the fetched item as response
      res.json(item);
    }
  } catch (err) {
    // Sending error message as response if an error occurs
    res.status(500).send(err.message);
  }
});


//--------------------- RestAPI for User registration and login --------------------
// Importing User model
const User = require('./Models/userModel');
// Importing bcrypt for password hashing
const bcrypt = require('bcrypt');
// Importing jsonwebtoken for generating tokens
const jwt = require('jsonwebtoken');

// Endpoint to register a user
app.post('/register', async (req, res) => {
  try {
    // Creating a new user with the data from request body
    const newUser = new User(req.body);
    // Saving the new user to the database
    await newUser.save();
    // Sending success message as response
    res.status(201).json({ message: 'User registered successfully!' });
  } catch (err) {
    // Sending error message as response if an error occurs
    res.status(500).send(err.message);
  }
});

// Endpoint to login a user
app.post('/login', async (req, res) => {
  try {
    // Extracting username and password from request body
    const { username, password } = req.body;
    // Fetching user from the database by username
    const user = await User.findOne({ username });
    // Checking if user exists and the password is correct
    if (!user || !(await bcrypt.compare(password, user.password))) {
      // Sending unauthorized message as response if username or password is invalid
      return res.status(401).json({ message: 'Invalid username or password' });
    }
    // Generating token for the user
    const token = jwt.sign({ id: user._id }, 'projet_pima_g8_crowdlending', { expiresIn: '1h' });
    // Sending token and user data as response
    res.json({
      token,
      user: {
        username: user.username,
        email: user.email,
        status: user.status
        // ... any other user info we want to include
      }
    });
  } catch (err) {
    // Sending error message as response if an error occurs
    res.status(500).json({ message: err.message });
  }
});

//--------------------- RestAPI to check if a valid token exist --------------------
app.post('/verifyToken', (req, res) => {
  const token = req.headers['authorization']?.split(' ')[1];
  if (!token) {
    return res.status(401).json({ message: 'No token provided' });
  }

  jwt.verify(token, 'projet_pima_g8_crowdlending', (err, decoded) => {
    if (err) {
      return res.status(401).json({ message: 'Invalid token' });
    }

    res.json({ message: 'Token is valid' });
  });
});

//-----API pour mettre à "borrowed" l'item -------------------
app.post('/items/borrow/:id', async (req, res) => {
  try {
    const itemID = req.params.id;
    const newItemStatus = 'Borrowed';

    // Mettre à jour le statut de l'objet dans la base de données en utilisant l'ID
    const updatedItem = await Item.findByIdAndUpdate(itemID, { status: newItemStatus }, { new: true });

    res.json(updatedItem);
  } catch (err) {
    res.status(500).send(err.message);
  }
});


app.post('/items', async (req, res) => {
  try {
    const { name, description, status, image, quantity } = req.body;
    const newItem = new Item({ name, description, status, image, quantity });
    await newItem.save();
    res.status(201).json(newItem);
  } catch (err) {
    res.status(500).send(err.message);
  }
});


// Endpoint pour mettre à jour le statut de l'objet de borrowed à available
app.post('/items/unborrow/:id', async (req, res) => {
  try {
    const itemID = req.params.id;
    const newItemStatus = 'Available';
    const newBorrowedBy = null; // Mise à jour de l'emprunteur à null
    const newLendingDate = null; // Mise à jour de la date d'emprunt à null

    // Mettre à jour le statut, l'emprunteur et la date d'emprunt de l'objet dans la base de données en utilisant l'ID
    const updatedItem = await Item.findByIdAndUpdate(itemID, { status: newItemStatus, borrowedBy: newBorrowedBy, lendingDate: newLendingDate }, { new: true });

    res.json(updatedItem);
  } catch (err) {
    res.status(500).send(err.message);
  }
});


// Endpoint pour obtenir la quantité actuelle de l'item par ID
app.get('/items/quantity/:id', async (req, res) => {
  try {
    const itemID = req.params.id;
    const item = await Item.findById(itemID);
    if (item) {
      res.json({ quantity: item.quantity });
    } else {
      res.status(404).send('Item not found');
    }
  } catch (err) {
    res.status(500).send(err.message);
  }
});


app.put('/items/:id/update-quantity-and-borrowed', async (req, res) => {
  try {
    const { id } = req.params;
    const { newQuantity, quantityBorrowed } = req.body;

    const item = await Item.findById(id);
    if (!item) {
      return res.status(404).send('Item not found');
    }

    // Mettez à jour la quantité de l'item
    item.quantity = newQuantity;
    // Mettez à jour quantityBorrowed
    item.quantityBorrowed = quantityBorrowed;

    await item.save();

    res.status(200).json(item);
  } catch (err) {
    res.status(500).send(err.message);
  }
});


// Route pour mettre à jour la date de rendu d'un article par son ID
app.put('/items/:id/update-lend-date', async (req, res) => {
  try {
    const { id } = req.params;
    const { lendDate } = req.body;

    const item = await Item.findById(id);
    if (!item) {
      return res.status(404).send('Item not found');
    }

    // Mettez à jour la date de rendu (lend_date) de l'item
    item.lend_date = lendDate;

    await item.save();

    res.status(200).json(item);
  } catch (err) {
    res.status(500).send(err.message);
  }
});


app.get('/items/:id/next-lend-date', async (req, res) => {
  try {
      const itemId = req.params.id;
      const borrowedItems = await BorrowedItem.find({ 
          id_item_borrowed: itemId,
          lend_date: { $gte: new Date() } // Utiliser lend_date au lieu de return_date
      }).sort({ lend_date: 1 }); // Trier par lend_date

      if (borrowedItems.length === 0) {
          return res.status(404).json({ message: 'Aucune date de prêt future trouvée pour cet objet.' });
      }

      const nextLendDate = borrowedItems[0].lend_date;

      res.json({ nextLendDate });
  } catch (err) {
      res.status(500).send(err.message);
  }
});



// Route pour mettre à jour la quantité empruntée d'un article par son ID
app.put('/items/:id/update-quantity-borrowed', async (req, res) => {
  try {
    const itemId = req.params.id;
    const newQuantityBorrowed = req.body.quantityBorrowed;

    // Recherchez l'article par son ID
    const item = await Item.findById(itemId);

    if (!item) {
      return res.status(404).json({ message: "Article not found" });
    }

    // Mettez à jour la quantité empruntée de l'article
    item.quantityBorrowed = newQuantityBorrowed;

    // Sauvegardez les modifications dans la base de données
    await item.save();

    return res.status(200).json({ message: "Quantité empruntée mise à jour avec succès", item });
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
});


//--------------------- RestAPI for retrieving Borroweditems from DB --------------------

const BorrowedItem = require('./Models/borroweditemModel');

// Endpoint to get all borrowed items
app.get('/borrowed-items', async (req, res) => {
  try {
    // Fetching all borrowed items from the database
    const borrowedItems = await BorrowedItem.find();
    // Sending the fetched borrowed items as response
    res.json(borrowedItems);
  } catch (err) {
    // Sending error message as response if an error occurs
    res.status(500).send(err.message);
  }
});

// Endpoint to create a borrowed item
app.post('/borrowed-items', async (req, res) => {
  try {
    const newBorrowedItem = new BorrowedItem(req.body);
    await newBorrowedItem.save();
    res.status(201).json(newBorrowedItem);
  } catch (err) {
    console.error("Error while saving borrowed item:", err);

    // Check if the error is a duplicate key error
    if (err.code === 11000) {
      // You can customize the message based on your application's needs
      return res.status(409).send('A borrowed item with the same identifier already exists.');
    }

    // For other types of errors, continue sending a generic error response
    res.status(500).send(err.message);
  }
});


// Endpoint to get a single borrowed item by ID
app.get('/borrowed-items/:id', async (req, res) => {
  try {
    // Fetching a borrowed item by ID from the database
    const borrowedItem = await BorrowedItem.findById(req.params.id);
    if (!borrowedItem) {
      // Sending not found message as response if no borrowed item is found
      res.status(404).send('Borrowed item not found');
    } else {
      // Sending the fetched borrowed item as response
      res.json(borrowedItem);
    }
  } catch (err) {
    // Sending error message as response if an error occurs
    res.status(500).send(err.message);
  }
});

// Endpoint to delete all borrowed items
app.delete('/borrowed-items', async (req, res) => {
  try {
    // Supprimer tous les éléments empruntés de la base de données
    await BorrowedItem.deleteMany({});
    // Répondre avec un message indiquant que tous les éléments ont été supprimés
    res.status(200).send('All borrowed items have been deleted');
  } catch (err) {
    // En cas d'erreur, renvoyer une réponse 500 (Erreur du serveur interne) avec le message d'erreur
    res.status(500).send(err.message);
  }
});

// Endpoint to delete a borrowed one by one
app.delete('/borrowed-items/:id', async (req, res) => {
  try {
    const borrowedItemId = req.params.id;
    // Supprimer tous les éléments empruntés de la base de données
    await BorrowedItem.findByIdAndDelete(borrowedItemId);
    // Répondre avec un message indiquant que tous les éléments ont été supprimés
    res.status(200).send(`Borrowed (1) item with ID ${borrowedItemId} has been deleted`);
  } catch (err) {
    // En cas d'erreur, renvoyer une réponse 500 (Erreur du serveur interne) avec le message d'erreur
    res.status(500).send(err.message);
  }
});

// -------------------------------------------------
// ----------------------ADMIN---------------------------
// -------------------------------------------------

const path = require('path');
const multer = require('multer');
// Configure multer
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/');
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});

const upload = multer({ storage: storage });

// Fetch all users
app.get('/admin/users', async (req, res) => {
  try {
    const users = await User.find();
    res.json(users);
  } catch (err) {
    res.status(500).send(err.message);
  }
});

// Fetch all items
app.get('/admin/items', async (req, res) => {
  try {
    const items = await Item.find();
    res.json(items);
  } catch (err) {
    res.status(500).send(err.message);
  }
});

// Add a new item
// Assuming multer is set up correctly
// Make sure to configure multer to handle file uploads as needed

app.post('/admin/items', upload.single('image'), async (req, res) => {
  try {
    const { name, description, quantity, status } = req.body;
    const newItem = new Item({
      name: name,
      description: description,
      quantity: quantity,
      status: status,
      // Assuming the image is uploaded and req.file contains the image information
      image: req.file ? req.file.path : null
    });

    await newItem.save();
    res.status(201).json({ message: 'Item added successfully', item: newItem });
  } catch (error) {
    res.status(500).json({ message: 'Error adding item', error: error.message });
  }
});


// Upload image
app.post('/admin/upload', upload.single('image'), async (req, res) => {
  try {
    // req.file contains the uploaded file
    // You might want to save the file path in the database or perform other operations
    res.json({ imagePath: req.file.path });
  } catch (err) {
    res.status(500).send(err.message);
  }
});

// Endpoint to get the count of users
app.get('/admin/usercount', async (req, res) => {
  try {
    const userCount = await User.countDocuments();
    res.json({ count: userCount });
  } catch (err) {
    res.status(500).send(err.message);
  }
});

// Endpoint to get the count of items
app.get('/admin/itemcount', async (req, res) => {
  try {
    const itemCount = await Item.countDocuments();
    res.json({ count: itemCount });
  } catch (err) {
    res.status(500).send(err.message);
  }
});


app.get('/admin/borrowedItemsCount', async (req, res) => {
  try {
    const borrowedItemsCount = await BorrowedItem.countDocuments();
    res.json({ count: borrowedItemsCount });
  } catch (err) {
    res.status(500).send(err.message);
  }
});

// Endpoint pour obtenir un item par son nom
app.get('/admin/items/name/:itemName', async (req, res) => {
  try {
    const itemName = req.params.itemName;
    const item = await Item.findOne({ name: itemName });
    if (!item) {
      return res.status(404).send('Item not found');
    }
    res.json(item);
  } catch (err) {
    res.status(500).send(err.message);
  }
});

// Endpoint pour supprimer un item par ID
app.delete('/admin/items/:id', async (req, res) => {
  try {
    const itemId = req.params.id;
    const item = await Item.findByIdAndDelete(itemId);
    console.log(item);
    if (!item) {
      return res.status(404).json({ message: 'Item not found' });
    }
    res.status(200).json({ message: `Item with ID ${itemId} has been deleted` });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

// app.put('/admin/itemsUpdate/:id', async (req, res) => {
//   try {
//     const itemId = req.params.id;
//     const updateData = req.body; // Or process FormData if you're sending files

//     const updatedItem = await Item.findByIdAndUpdate(itemId, updateData);
//     if (!updatedItem) {
//       return res.status(404).send('Item not found');
//     }

//     res.json({updatedItem, updateData});
//   } catch (err) {
//     res.status(500).send(err.message);
//   }
// });

app.put('/admin/itemsUpdate/:id', upload.single('image'), async (req, res) => {
  try {
    const itemId = req.params.id;
    const updateData = {
      description: req.body.description,
      quantity: req.body.quantity,
      // handle other fields as necessary multer
    };

    if (req.file) {
      updateData.image = req.file.path; // or any other way you want to store the file path
    }

    const updatedItem = await Item.findByIdAndUpdate(itemId, updateData);
    if (!updatedItem) {
      return res.status(404).send('Item not found');
    }

    res.json(updatedItem);
  } catch (err) {
    res.status(500).send(err.message);
  }
});


// Endpoint pour rechercher tout l'historique 
const Historique = require('./Models/historyModel');

app.get('/admin/history/all', async (req, res) => {
  try {
    const historique = await Historique.find();
    res.json(historique);
  } catch (err) {
    res.status(500).send(err.message);
  }
})

// Endpoint pour rechercher l'historique par ID
app.get('/admin/history/:id', async (req, res) => {
  try {
    const historiqueId = req.params.id;
    const historique = await Historique.findById(historiqueId);
    if (!historique) {
      return res.status(404).send('Historique not found');
    }
    res.json(historique);
  } catch (err) {
    res.status(500).send(err.message);
  }
});

// Endpoint pour modifier ajouter un historique
app.post('/admin/history/add', async (req, res) => {
  try {
    const { user, date, description, type, validated, quantity } = req.body;
    const newHistorique = new Historique({ user, date, description, type, validated, quantity });
    await newHistorique.save();
    res.status(201).json(newHistorique);
  } catch (err) {
    res.status(500).send(err.message);
  }
});

app.delete('/admin/history/delete/:id', async (req, res) => {
  try {
    const historiqueId = req.params.id;
    const historique = await Historique.findByIdAndDelete(historiqueId);
    if (!historique) {
      return res.status(404).json({ message: 'Historique not found' });
    }
    res.status(200).json({ message: `Historique with ID ${historiqueId} has been deleted` });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

// Endpoint pour modifier un historique
app.put('/admin/history/update/:id', async (req, res) => {
  try {
    const historiqueId = req.params.id;
    const { user, date, description, type, validated, quantity } = req.body;

    const historique = await Historique.findById(historiqueId);
    if (!historique) {
      return res.status(404).json({ message: 'Historique not found' });
    }

    historique.user = user;
    historique.date = date;
    historique.description = description;
    historique.type = type;
    historique.validated = validated;
    historique.quantity = quantity;

    await historique.save();

    res.status(200).json(historique);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

app.delete('/admin/history/delete', async (req, res) => {
  try {
    // delete all history from the database;
    await Historique.deleteMany({});
    res.status(200).send('All history have been deleted');
  } catch (err) {
    res.status(500).send(err.message);
  }
})


// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
