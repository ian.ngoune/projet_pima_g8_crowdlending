document.addEventListener('DOMContentLoaded', () => {
    const token = localStorage.getItem('token');
    if (!token) {
        // Redirect to login page if no token is found
        window.location.href = '../login.html';
    } else {
        // You can also send the token to the server to verify it's still valid
        fetch('http://localhost:3000/verifyToken', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Token invalid');
                }
                return response.json();
            })
            .then(data => {
                console.log('Token is valid');
                // Load the main page content
                fetchUsers();
                fetchItems();
                fetchUserCount();
                fetchItemCount();
                fetchborrowedItemsCount();
                fetchHistory();

                let user = JSON.parse(localStorage.getItem('user')); // Recupération des informations de l'utilisateur stockées dans le local storage
                let user_comp = document.getElementById("user_id"); // Recupération de l'élément HTML où afficher le nom de l'utilisateur
                user_comp.textContent = user.username; // Affichage du nom de l'utilisateur

            })
            .catch(error => {
                console.log(error.message);
                // Redirect to login page if token is invalid
                window.location.href = '../login.html';
            });
    }
});

// Function to fetch and display users
function fetchUsers() {
    fetch('http://localhost:3000/admin/users')
        .then(response => response.json())
        .then(users => {
            const usersTableBody = document.getElementById('usersTable').querySelector('tbody');
            users.forEach(user => {
                let row = usersTableBody.insertRow();
                row.innerHTML = `
                    <td>${user._id}</td>
                    <td>${user.username}</td>
                    <td>${user.status}</td>
                    <!-- Other fields as needed -->
                `;
            });
        })
        .catch(error => console.error('Error:', error));
}

// Function to fetch and display items
function fetchItems() {
    fetch('http://localhost:3000/admin/items')
        .then(response => response.json())
        .then(items => {
            const itemsTableBody = document.getElementById('itemsTable').querySelector('tbody');
            items.forEach(item => {
                // Check if the image path starts with 'uploads'
                const imageUrl = item.image.startsWith('uploads/')
                    ? `../${item.image}`
                    : item.image;


                let row = itemsTableBody.insertRow();
                row.innerHTML = `
                    <td><img src="${imageUrl}" alt="${item.name}" style="width: 100px; height: auto;"></td>
                    <td>${item.name}</td>
                    <td>${item.description}</td>
                    <td>${item.quantity}</td>
                    <td>${item.status}</td>
                    <!-- Other fields as needed -->
                `;
            });
        })
        .catch(error => console.error('Error:', error));
}

// Add Item Form Submission
document.getElementById('addItemForm').addEventListener('submit', function (e) {
    e.preventDefault();

    const quantity = document.getElementById('itemQuantity').value;
    if (quantity < 0) {
        alert('Quantity cannot be negative.');
        return; // Stop the form submission
    }

    const formData = new FormData();
    formData.append('name', document.getElementById('itemName').value);
    formData.append('description', document.getElementById('itemDescription').value);
    formData.append('quantity', document.getElementById('itemQuantity').value);
    formData.append('status', document.getElementById('itemStatus').checked ? 'Available' : 'Unavailable');
    formData.append('image', document.getElementById('itemImage').files[0]);

    fetch('http://localhost:3000/admin/items', {
        method: 'POST',
        body: formData
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            alert('Item added successfully');
            // Clear the form after successful submission
            document.getElementById('addItemForm').reset();

            const imagePreview = document.getElementById('imagePreview');

            imagePreview.style.display = 'none';

            // Optionally, you can refresh the items list if it's being displayed on the dashboard
            location.reload();
        })
        .catch(error => {
            console.error('Error:', error);
            alert('Error adding item');
        });
});


function submitUserForm() {
    const userName = document.getElementById('userName').value;
    const userPassword = document.getElementById('userPassword').value;
  
    const userData = {
      username: userName,
      password: userPassword,
      status: "User" // Set status as required
    };
  
    fetch('http://localhost:3000/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(userData),
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then(data => {
      console.log('User added:', data);
      // Handle success - e.g., clear the form, show a success message
      window.location.reload();
    })
    .catch(error => {
      console.error('Error adding user:', error);
      // Handle error - e.g., show an error message
    });
  }
  

// Remove Item Form Submission
document.getElementById('removeItemForm').addEventListener('submit', function (e) {
    e.preventDefault();

    const itemName = document.getElementById('itemRemoveName').value;

    if (!itemName) {
        alert('Please enter a valid item name.');
        return;
    }

    // Première requête pour obtenir l'ID de l'article
    fetch(`http://localhost:3000/admin/items/name/${encodeURIComponent(itemName)}`, {
        method: 'GET'
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Item not found');
            }
            return response.json();
        })
        .then(data => {
            if (!data._id) {
                throw new Error('No ID found for the item');
            }
            // Deuxième requête pour supprimer l'article
            return fetch(`http://localhost:3000/admin/items/${data._id}`, {
                method: 'DELETE'
            });
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Error occurred while removing the item');
            }
            return response.json();
        })
        .then(data => {
            alert('Item removed successfully');
            document.getElementById('removeItemForm').reset();
            location.reload();
        })
        .catch(error => {
            console.error('Error:', error);
            alert(error.message);
        });
});

document.getElementById('updateItemForm').addEventListener('submit', function (e) {
    e.preventDefault();
  
    const itemId = document.getElementById('updateItemSelect').value;
    if (!itemId) {
      alert('Veuillez sélectionner un objet.');
      return;
    }
  
    const formData = new FormData();
    formData.append('description', document.getElementById('updateItemDescription').value);
    formData.append('quantity', document.getElementById('updateItemQuantity').value);
    if (document.getElementById('updateItemImage').files[0]) {
      formData.append('image', document.getElementById('updateItemImage').files[0]);
    }
  
    // Debugging: Inspect FormData contents
    formData.forEach((value, key) => console.log(key, value));
  
    fetch(`http://localhost:3000/admin/itemsUpdate/${itemId}`, {
      method: 'PUT',
      body: formData
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then(data => {
        console.log(data)
      alert('Item updated successfully');
      document.getElementById('updateItemForm').reset();
      location.reload();
    })
    .catch(error => {
      console.error('Error:', error);
      alert('Error updating item');
    });
  });
  

function fetchAndFillItemsSelect() {
    fetch('http://localhost:3000/admin/items')
      .then(response => response.json())
      .then(items => {
        const updateItemSelect = document.getElementById('updateItemSelect');
        items.forEach(item => {
          let option = document.createElement('option');
          option.value = item._id;
          option.textContent = item.name;
          updateItemSelect.appendChild(option);
        });
      })
      .catch(error => console.error('Error:', error));
}

fetchAndFillItemsSelect();
  

function fetchUserCount() {
    fetch('http://localhost:3000/admin/usercount')
        .then(response => response.json())
        .then(data => {
            document.getElementById('usersCount').textContent = data.count;
        })
        .catch(error => console.error('Error:', error));
}

function fetchItemCount() {
    fetch('http://localhost:3000/admin/itemcount')
        .then(response => response.json())
        .then(data => {
            document.getElementById('itemsCount').textContent = data.count;
        })
        .catch(error => console.error('Error:', error));
}

function fetchborrowedItemsCount() {
    fetch('http://localhost:3000/admin/borrowedItemsCount')
        .then(response => response.json())
        .then(data => {
            document.getElementById('borrowedItemsCount').textContent = data.count;
        })
        .catch(error => console.error('Error:', error));
}

// Fonction qui gère la déconnexion
function logout() {
    localStorage.removeItem('token'); // Suppression du token
    localStorage.removeItem('user');  // Suppression des informations de l'utilisateur
    window.location.href = '../login.html'; // Redirection vers la page de connexion

}

// Fonction pour basculer l'affichage du formulaire addItemForm
function toggleAddItemForm() {
    var addItemForm = document.getElementById('addItemForm').parentNode.parentNode;
    var removeItemForm = document.getElementById('removeItemForm').parentNode.parentNode;
    var updateItemForm = document.getElementById('updateItemForm').parentNode.parentNode;
    removeItemForm.style.display = 'none';
    updateItemForm.style.display = 'none';
    if (addItemForm.style.display === 'none' || addItemForm.style.display === '') {
        addItemForm.style.display = 'block';
    } else {
        addItemForm.style.display = 'none';
    }
}

// Fonction pour basculer l'affichage du formulaire removeItemForm
function toggleRemoveItemForm() {
    var addItemForm = document.getElementById('addItemForm').parentNode.parentNode;
    var removeItemForm = document.getElementById('removeItemForm').parentNode.parentNode;
    var updateItemForm = document.getElementById('updateItemForm').parentNode.parentNode;
    addItemForm.style.display = 'none';
    updateItemForm.style.display = 'none';
    if (removeItemForm.style.display === 'none' || removeItemForm.style.display === '') {
        removeItemForm.style.display = 'block';
    } else {
        removeItemForm.style.display = 'none';
    }
}

// Fonction pour basculer l'affichage du formulaire removeItemForm
function toggleUpdateItemForm() {
    var addItemForm = document.getElementById('addItemForm').parentNode.parentNode;
    var removeItemForm = document.getElementById('removeItemForm').parentNode.parentNode;
    var updateItemForm = document.getElementById('updateItemForm').parentNode.parentNode;
    addItemForm.style.display = 'none';
    removeItemForm.style.display = 'none';
    if (updateItemForm.style.display === 'none' || updateItemForm.style.display === '') {
        updateItemForm.style.display = 'block';
    } else {
        updateItemForm.style.display = 'none';
    }
}


// Cacher les formulaires initialement
window.onload = function () {
    var addItemForm = document.getElementById('addItemForm').parentNode.parentNode;
    addItemForm.style.display = 'none';
    var removeItemForm = document.getElementById('removeItemForm').parentNode.parentNode;
    removeItemForm.style.display = 'none';
    var updateItemForm = document.getElementById('updateItemForm').parentNode.parentNode;
    updateItemForm.style.display = 'none';
};

// Function to fetch and display history
function fetchHistory() {
    fetch('http://localhost:3000/admin/history/all')
        .then(response => response.json())
        .then(history => {
            // trier la liste d'historique par date dans l'ordre décroissant
            return history.sort((a, b) => (a.date > b.date) ? -1 : 1);
        })
        .then(historysort => {
            const historyTableBody = document.getElementById('historyTable').querySelector('tbody');
            historysort.forEach(historyItem => {
                let row = historyTableBody.insertRow();
                let date = new Date(historyItem.date);
                row.innerHTML = `
                    <th scope="row">${historyItem._id}</td>
                    <td>${historyItem.user}</td>
                    <td>${date.toLocaleDateString()}</td>
                    <td>${date.toLocaleTimeString()}</td>
                    <td>${historyItem.description}</td>
                    <td>${historyItem.type}</td>
                    <td>${historyItem.validated}</td>
                    <td>${historyItem.quantity}</td>
                    <!-- Other fields as needed -->
                `;
            });
            console.log("test affichage historique");
            console.log(historysort);
        })
        .catch(error => console.error('Error:', error));
}

// Mettre un limite pour l'update d'objet
document.getElementById('updateItemSelect').addEventListener('change', function(e) {
    const itemId = e.target.value;
  
    fetch(`http://localhost:3000/items/quantityBorrowed/${itemId}`)
      .then(response => response.json())
      .then(data => {
        const minQuantity = data.quantityBorrowed;
        document.getElementById('updateItemQuantity').min = minQuantity;
      })
      .catch(error => console.error('Error:', error));
  });
  
// Example Function to Add a User

  
  // Implement other functions as needed for fetching and displaying users addItemForm
  