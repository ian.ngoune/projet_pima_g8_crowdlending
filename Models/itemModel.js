const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  quantity: {
    type: Number,
    default: "6",
    
  },
  quantityBorrowed: {
    type: Number,
    default: "0",
  },
  lend_date: {
    type: Date,
    default: null,
  },
});

const Item = mongoose.model('Item', itemSchema);

module.exports = Item;
