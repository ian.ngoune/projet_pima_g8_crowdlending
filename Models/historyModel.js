const mongoose = require('mongoose'); // On importe mongoose

const historiqueSchema = new mongoose.Schema({ // On crée un nouveau schéma pour l'historique
    user: { // On crée un champ user
        type: String, // Le type de données pour l'utilisateur (peut être ObjectId si vous avez un modèle d'utilisateur)
        required: true, // On rend le champ obligatoire
    },
    date: { // On crée un champ date
        type: Date, // Le type de données pour la date
        required: true, // On rend le champ obligatoire
    },
    description: { // On crée un champ description
        type: String, // Le type de données pour la description
        required: true, // On rend le champ obligatoire
    },
    type: { // On crée un champ type
        type: String, // Le type de données pour le type
        required: true, // On rend le champ obligatoire
    },
    validated: { // On crée un champ validated
        type: Boolean, // Le type de données pour la validation
        required: true, // On rend le champ obligatoire
    },
    quantity: { // On crée un champ quantity
        type: Number, // Le type de données pour la quantité
        required: true, // On rend le champ obligatoire
    },
});

const Historique = mongoose.model('Historique', historiqueSchema); // On crée le modèle Historique

module.exports = Historique; // On exporte le modèle Historique