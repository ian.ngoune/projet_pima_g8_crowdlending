const mongoose = require('mongoose');

const borrowedItemSchema = new mongoose.Schema({
    id_item_borrowed: {
        type: String, // Ou le type de données approprié pour votre identifiant
        required: false,
    },
    user: {
        type: String, // Le type de données pour l'utilisateur (peut être ObjectId si vous avez un modèle d'utilisateur)
        required: true,
    },
    name: {
        type: String, // Le type de données pour le nom
        required: true,
    },
    quantity: {
        type: Number,
        required: true,
    },
    borrow_date: {
        type: Date,
        required: true,
    },
    lend_date: {
        type: Date,
        required: true,
    },
});

const BorrowedItem = mongoose.model('BorrowedItem', borrowedItemSchema);

module.exports = BorrowedItem;

