let items = [
    {
        id: 1,
        name: "Laptop",
        description: "MacBook Pro",
        status: "Available",
        image: "assets/image/laptop.jpeg",
        borrower : null,
    },
    {
        id: 2,
        name: "Hair dryer",
        description: "seche cheveux",
        status: "Borrowed",
        image: "assets/image/seche_ch.jpg",
        borrower : null,
    },
    {
        id: 3,
        name: "Projector",
        description: "Epson Full HD Projector - Model E456",
        status: "Available",
        image: "assets/image/projector.png",
        borrower : null,
    },
    {
        id: 4,
        name: "Board Game",
        description: "Monopoly - Classic Edition",
        status: "Borrowed",
        image: "assets/image/monopoly.jpg",
        borrower : null,
    },
];
