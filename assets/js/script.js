document.addEventListener('DOMContentLoaded', () => {
    const token = localStorage.getItem('token');
    if (!token) {
        // Redirect to login page if no token is found
        window.location.href = 'login.html';
    } else {
        // You can also send the token to the server to verify it's still valid
        fetch('http://localhost:3000/verifyToken', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Token invalid');
                }
                return response.json();
            })
            .then(data => {
                console.log('Token is valid');
                // Load the main page content
            })
            .catch(error => {
                console.log(error.message);
                // Redirect to login page if token is invalid
                window.location.href = 'login.html';
            });
    }
});



document.addEventListener("DOMContentLoaded", function () {
    populateDropdown();
});

document.addEventListener("DOMContentLoaded", function () {
    let user = JSON.parse(localStorage.getItem('user')); // Recupération des informations de l'utilisateur stockées dans le local storage
    let user_comp = document.getElementById("user_id"); // Recupération de l'élément HTML où afficher le nom de l'utilisateur
    user_comp.textContent = user.username; // Affichage du nom de l'utilisateur

    fetch('http://localhost:3000/borrowed-items')
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(items => {
            let borrowedItems = items.filter(item => item.user === user.username);
            let borrowedItemsCount = borrowedItems.length;
            let bdg = document.getElementById("bdg");
            bdg.textContent = borrowedItemsCount;
        })
}
)

// Fonction qui gère la déconnexion
function logout() {
    localStorage.removeItem('token'); // Suppression du token
    localStorage.removeItem('user');  // Suppression des informations de l'utilisateur
    window.location.href = 'login.html'; // Redirection vers la page de connexion

}

//WITHOUT DB 
// function populateDropdown() {
//     let dropdown = document.getElementById("itemSelect");

//     items.forEach(item => {
//         let option = document.createElement("option");
//         option.value = item.id;
//         option.textContent = item.name;
//         dropdown.appendChild(option);
//     });
// }

// WITH DB
function populateDropdown() {
    fetch('http://localhost:3000/items')
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(items => {
            let dropdown = document.getElementById("itemSelect");

            items.forEach(item => {
                let option = new Option(item.name, item._id);
                dropdown.appendChild(option);
            });
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });
}



function displayItemDetails() {
    let dropdown = document.getElementById("itemSelect");
    let selectedID = dropdown.value;

    if (selectedID === "Select an item") {
        document.getElementById("noSelectItem").classList.remove("d-none");
        document.getElementById("noSelectItem").textContent = "No item selected";
        document.getElementById("borrowBtn").classList.add("d-none");
        document.getElementById("itemName").classList.add("d-none");
        document.getElementById("itemDescription").classList.add("d-none");
        document.getElementById("stateStatus").classList.add("d-none");
        document.getElementById("itemStatus").classList.add("d-none");
        document.getElementById("itemImage").classList.add("d-none");


        // Masquer les éléments
        document.querySelectorAll('.hide-by-default').forEach(element => {
            element.style.display = 'none';
        });
        return; // Exit early if the default option is selected


    }

    // let selectedItem = items.find(item => item.id == selectedID);

    fetch(`http://localhost:3000/items/${selectedID}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(selectedItem => {
            if (selectedItem) {

                // Afficher les éléments "+" "-" "quantité" etc..
                document.querySelectorAll('.hide-by-default').forEach(element => {
                    element.style.display = 'block';
                });
                document.getElementById("borrowBtn").classList.remove("d-none");
                document.getElementById("itemName").classList.remove("d-none");
                document.getElementById("itemDescription").classList.remove("d-none");
                document.getElementById("stateStatus").classList.remove("d-none");
                document.getElementById("itemStatus").classList.remove("d-none");
                document.getElementById("itemImage").classList.remove("d-none");
                // document.getElementById("noSelectItem").textContent = "";
                document.getElementById("noSelectItem").classList.add("d-none");
                document.getElementById("itemName").textContent = selectedItem.name;
                document.getElementById("itemDescription").textContent = selectedItem.description;
                document.getElementById("stateStatus").textContent = "Status: ";


                // Set the badge content and classes based on status
                let itemStatusBadge = document.getElementById("itemStatus");
                itemStatusBadge.textContent = selectedItem.status;
                if (selectedItem.status === "Available") {
                    itemStatusBadge.classList.add("badge-success");
                    itemStatusBadge.classList.remove("badge-danger");
                } else {
                    itemStatusBadge.classList.add("badge-danger");
                    itemStatusBadge.classList.remove("badge-success");
                }

                document.getElementById("itemImage").src = selectedItem.image;

                let borrowBtn = document.getElementById("borrowBtn");

                // Display the button
                borrowBtn.classList.remove("d-none");

                if (selectedItem.status === "Borrowed") {
                    if (selectedItem.quantity == 0) {
                        fetch(`http://localhost:3000/items/${selectedID}/next-lend-date`)
                            .then(res => res.json())
                            .then(data => {
                                if (data.nextLendDate) {
                                    const nextLendDate = new Date(data.nextLendDate).toLocaleDateString();
                                    document.getElementById('nextLendDateDisplay').textContent = `Prochainement disponible le : ${nextLendDate}`;
                                } else {
                                    document.getElementById('nextLendDateDisplay').textContent = 'Prochaine date de prêt inconnue';
                                }
                            })
                        borrowBtn.setAttribute("disabled", "disabled");
                        borrowBtn.classList.add("btn-secondary");
                    }

                } else {
                    document.getElementById('nextLendDateDisplay').textContent = '';

                    borrowBtn.classList.remove("btn-secondary");
                    borrowBtn.removeAttribute("disabled");

                }


            }
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });
}


// Obtenez les éléments du texte de la quantité disponible et du message d'indisponibilité
const quantityAvailable = document.getElementById("quantityAvailable");
const unavailableMessage = document.getElementById("unavailableMessage");

// Écoutez le changement de sélection dans la liste déroulante
document.getElementById("itemSelect").addEventListener("change", function () {
    const selectedID = this.value;

    // Vérifiez si une option autre que "Select an item" est sélectionnée
    if (selectedID !== "Select an item") {
        // Mettez à jour la quantité disponible en fonction de l'ID de l'item
        fetch(`http://localhost:3000/items/quantity/${selectedID}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                const quantity = data.quantity;

                // Mettez à jour la quantité disponible
                quantityAvailable.textContent = quantity;

                // Affichez le message d'indisponibilité si la quantité est égale à 0
                if (quantity === 0) {
                    unavailableMessage.textContent = "Cet objet n'est plus disponible";
                } else {
                    unavailableMessage.textContent = "";
                }
            })
            .catch(error => {
                console.error('There has been a problem with your fetch operation:', error);
            });
    } else {
        // Si "Select an item" est sélectionné, réinitialisez la quantité disponible et le message d'indisponibilité
        quantityAvailable.textContent = "";
        unavailableMessage.textContent = "";
    }
});

// Définir la fonction de gestionnaire d'événements à l'extérieur du bloc else
function borrowBtnClickHandler() {
    let dropdown = document.getElementById("itemSelect");
    let selectedID = dropdown.value;

    ChangeItemStatus(selectedID);
}
document.addEventListener("DOMContentLoaded", function () {
    // setupFormSubmitHandler();
});

function setBorrowedWithQantityEqualToZero(selectedID, lendDate) {

    fetch(`http://localhost:3000/items/borrow/${selectedID}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            status: 'Borrowed',
        }),
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            // Mettre à jour l'affichage ou effectuer d'autres actions si nécessaire
            console.log('Statut mis à jour avec succès :', data);

            fetch(`http://localhost:3000/items/${selectedID}/update-lend-date`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ lendDate: lendDate }),
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json();
                })
                .then(data => {
                    // Mettre à jour l'affichage ou effectuer d'autres actions si nécessaire
                    console.log('lend_date mis à jour avec succès :', data);
                    // Réafficher les détails de l'objet

                    displayItemDetails();
                })
                .catch(error => {
                    console.error('There has been a problem with your fetch operation:', error);
                });
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });
}



function ChangeItemStatus(selectedID) {
    // Récupérez la quantité actuelle de l'item
    fetch(`http://localhost:3000/items/quantity/${selectedID}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            const currentQuantity = data.quantity;

            // Récupérez la quantité que vous souhaitez emprunter depuis l'interface utilisateur
            const quantityInput = document.getElementById("quantity");
            const quantityToBorrow = parseInt(quantityInput.value, 10);

            // Récupérez la date de rendu depuis l'interface utilisateur
            const dateRenduInput = document.getElementById("date_rendu").value;

            if (!dateRenduInput) {
                alert("Veuillez sélectionner une date de rendu");
                return; // Arrêtez l'exécution de la fonction
            }

            const currentDate = new Date(); // Date actuelle
            const maxLendDate = new Date();
            maxLendDate.setDate(currentDate.getDate() + 14); // Définir la date maximale de rendu (14 jours après aujourd'hui)
            // Convertissez la date de rendu en objet Date
            const lendDate = new Date(dateRenduInput);

            if (lendDate > maxLendDate) {
                alert('borrow is too long');
                return;
            }

            if (lendDate < currentDate) {
                alert("Veuillez choisir une date correcte");
                return; // Arrêtez l'exécution de la fonction
            } else {
                try {
                    redirectToBorrowForm(selectedID);
                } catch (errors) {
                    console.log("+++++++++++++++++++++" + errors)
                }
                // Mise à jour de lend_date et statut dans la base de données
                if (quantityToBorrow == currentQuantity) {
                    //alert("test");
                    setBorrowedWithQantityEqualToZero(selectedID, lendDate);
                    // Mise à jour de la quantité et quantityBorrowed
                    fetch(`http://localhost:3000/items/${selectedID}/update-quantity-and-borrowed`, {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            newQuantity: currentQuantity - quantityToBorrow,
                            quantityBorrowed: quantityToBorrow,
                        }),
                    })
                        .then(response => {
                            if (!response.ok) {
                                throw new Error('Network response was not ok');
                            }
                            return response.json();
                        })
                        .then(data => {
                            // Mettre à jour la quantité dans la base de données
                            console.log('Statut et quantité mis à jour avec succès :', data);
                            // Réafficher les détails de l'objet
                            //displayItemDetails();    

                        })
                        .catch(error => {
                            console.error('There has been a problem with your fetch operation:', error);
                        });
                }
                else {
                    //stillAvailable(selectedID,lendDate);

                    // Mise à jour de la quantité et quantityBorrowed
                    fetch(`http://localhost:3000/items/${selectedID}/update-quantity-and-borrowed`, {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            newQuantity: currentQuantity - quantityToBorrow,
                            quantityBorrowed: quantityToBorrow,
                        }),
                    })
                        .then(response => {
                            if (!response.ok) {
                                throw new Error('Network response was not ok');
                            }
                            return response.json();
                        })
                        .then(data => {
                            // Mettre à jour la quantité dans la base de données
                            console.log('Statut et quantité mis à jour avec succès :', data);

                            // Réafficher les détails de l'objet
                            //displayItemDetails();

                        })
                        .catch(error => {
                            console.error('There has been a problem with your fetch operation:', error);
                        });
                }
            }

        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });



}

function getItemName(selectedID) {
    return new Promise((resolve, reject) => {
        fetch(`http://localhost:3000/items/${selectedID}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(item => {
                const itemName = item.name;
                resolve(itemName);
                console.log(typeof itemName);
                console.log("NOM : " + itemName);
            })
            .catch(error => {
                reject(error);
            });
    });
}


function redirectToBorrowForm(selectedID) {
    // Récupérez la quantité actuelle de l'item
    fetch(`http://localhost:3000/items/quantity/${selectedID}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            const currentQuantity = data.quantity;

            // Récupérez la quantité que vous souhaitez emprunter depuis l'interface utilisateur
            const quantityInput = document.getElementById("quantity");
            const quantityToBorrow = parseInt(quantityInput.value, 10);

            // Récupérez la date de rendu depuis l'interface utilisateur
            const dateRenduInput = document.getElementById("date_rendu").value;

            const currentDate = new Date(); // Date actuelle
            const maxLendDate = new Date();
            maxLendDate.setDate(currentDate.getDate() + 14); // Définir la date maximale de rendu (14 jours après aujourd'hui)
            // Convertissez la date de rendu en objet Date
            const lendDate = new Date(dateRenduInput);

            if (lendDate > maxLendDate) {
                alert('borrow is too long');
                return;
            }


            // Convertissez la date de rendu en objet Date
            const userString = localStorage.getItem("user");
            const userObject = JSON.parse(userString);
            const username = userObject.username;

            const itemName = getItemName(selectedID)
            // Création d'un nouvel élément emprunté
            getItemName(selectedID).then(itemName => {
                const borrowedItem = {
                    id_item_borrowed: selectedID, // ID de l'élément d'origine
                    user: username, // ID de l'utilisateur (vous devez le récupérer depuis le local storage)
                    name: itemName,
                    quantity: quantityToBorrow, // Quantité empruntée
                    borrow_date: currentDate, // Date d'emprunt (date actuelle)
                    lend_date: lendDate, // Date de rendu
                };

                //console.log("nom : "+ localStorage.getItem("user"));

                // alert('ok0');

                // Création de l'élément emprunté dans la base de données
                fetch(`http://localhost:3000/borrowed-items`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(borrowedItem),
                })
                    .then(response => {
                        // alert("errror" + borrowedItem)
                        // console.log("------------------>errror" + JSON.stringify(borrowedItem))
                        // console.log("------------------>response " + response.ok)
                        if (!response.ok) {
                            throw new Error('Network response was not ok');
                        }
                        // alert('ok1');
                        return response.json();
                    })
                    .then(data => {
                        // Mettre à jour l'affichage ou effectuer d'autres actions si nécessaire
                        // alert('ok ok');

                        console.log('Élément emprunté créé avec succès :', data);
                        alert("Vous avez bien emprunté l'objet");
                        // Réafficher les détails de l'objet
                        displayItemDetails();

                        // Ajouter l'opération à l'historique
                        let tmp_date = new Date();
                        const historyItem = {
                            user: username,
                            date: tmp_date,
                            description: `Item borrowed: ${itemName}`,
                            type: 'borrow',
                            validated: true,
                            quantity: quantityToBorrow
                        };

                        fetch(`http://localhost:3000/admin/history/add`, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify(historyItem),
                        })
                            .then(response => {
                                if (!response.ok) {
                                    throw new Error('Network response was not ok');
                                }
                                return response.json();
                            })
                            .then(data => {
                                console.log('Historique créé avec succès :', data);
                                window.location.reload();

                            })
                            .catch(error => {
                                console.error('There has been a problem with your fetch operation:2', error);
                            });
                    })

                    .catch(error => {
                        console.error('Il y a eu un problème avec votre requête :', error);
                    });
            })

                .catch(error => {
                    console.error('There has been a problem with your fetch operation:', error);
                });
        });

}


//Supprimes tous les items empruntés de la base de donnée
function resetBorrowedItems() {
    fetch('http://localhost:3000/borrowed-items', {
        method: 'DELETE',
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            // Réinitialisation réussie, vous pouvez effectuer des actions supplémentaires si nécessaire
            console.log('Borrowed items database has been reset');
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });
}

// Function to reset (delete) a borrowed item by ID
function resetBorrowedItemById(itemId) {
    fetch(`http://localhost:3000/borrowed-items/${itemId}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
        },
    })
        .then(response => {
            if (!response.ok) {
                // If the response is not OK, throw an error
                throw new Error('Network response was not ok');
            }
            // Log a success message if the deletion is successful
            console.log(`Borrowed item with ID ${itemId} has been deleted`);
        })
        .catch(error => {
            // Log an error message if there's a problem with the fetch operation
            console.error('There has been a problem with your fetch operation:', error);
        });
}


function redirectToUnborrowForm(selectedID) {
    // D'abord, obtenir les détails actuels de l'objet pour calculer la nouvelle quantité
    fetch(`http://localhost:3000/items/${selectedID}`)
        .then(response => response.json())
        .then(item => {
            // Effectuer la mise à jour de l'état de l'objet en 'Available'
            fetch(`http://localhost:3000/items/unborrow/${selectedID}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ status: 'Available' }),
            })
                .then(checkResponse)
                .then(data => {
                    console.log('Statut mis à jour avec succès :', data);
                    // Réafficher les détails de l'objet
                    displayItemDetails();
                })
                .catch(console.error);

            // Mise à jour de la quantité et quantityBorrowed
            fetch(`http://localhost:3000/items/${selectedID}/update-quantity-and-borrowed`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    newQuantity: item.quantity + item.quantityBorrowed,
                    quantityBorrowed: 0,
                }),
            })
                .then(checkResponse)
                .then(data => {
                    console.log('Quantité mise à jour avec succès :', data);
                    // Réafficher les détails de l'objet
                    displayItemDetails();
                })
                .catch(console.error);

        })
        .catch(console.error);
}

function checkResponse(response) {
    if (!response.ok) {
        throw new Error('Network response was not ok');
    }
    return response.json();
}

//Mes objets empruntés (MAJID)

document.getElementById('cartIcon').addEventListener('click', openMyItemsPopup);




function displayBorrowedItems() {
    // Récupérez l'utilisateur actuellement connecté
    const userString = localStorage.getItem("user");
    const userObject = JSON.parse(userString);
    const username = userObject.username;

    // Récupération des objets empruntés depuis l'API
    fetch('http://localhost:3000/borrowed-items')
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(items => {
            // Filtrer les objets empruntés pour cet utilisateur
            const userBorrowedItems = items.filter(item => item.user === username);

            const itemList = document.getElementById('borrowedItemsList');
            itemList.innerHTML = ''; // On vide la liste pour la remplir avec les nouvelles données

            userBorrowedItems.forEach(item => {

                let listItem = document.createElement('li');
                listItem.classList.add('list-group-item');

                // Créer une case à cocher à droite de chaque item emprunté
                let checkbox = document.createElement('input');
                checkbox.type = 'checkbox';
                checkbox.id = item._id;
                checkbox.name = item.name;
                checkbox.style.marginRight = '10px';

                // Créer un label pour la case à cocher que l'on va gérer à part
                let label = document.createElement('label');
                label.htmlFor = checkbox.id;
                label.textContent = `${item.name} (Quantité : ${item.quantity})`;

                listItem.appendChild(checkbox);
                listItem.appendChild(label);
                itemList.appendChild(listItem);
            });
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });
}








// Ouvre la popup des objets empruntés
function openMyItemsPopup() {
    let popup = document.getElementById('myItemsPopup');
    let background = document.getElementById('popupBackground');

    popup.style.display = "block";
    background.style.display = "block";


    displayBorrowedItems();
}

// Ferme la popup des objets empruntés
function closeMyItemsPopup() {
    let popup = document.getElementById('myItemsPopup');
    let background = document.getElementById('popupBackground');

    popup.style.display = "none";
    background.style.display = "none";
}

// Selectionne tout les élèments 
function selectAll() {
    // Sélectionnez toutes les cases à cocher dans la liste
    const checkboxes = document.querySelectorAll('#borrowedItemsList input[type="checkbox"]');

    // Boucle à travers chaque case à cocher et la cocher
    checkboxes.forEach(checkbox => {
        checkbox.checked = true;
    });

    // Masquer le bouton "Select All" et afficher le bouton "Deselect All"
    document.getElementById('selectAllButton').style.display = 'none';
    document.getElementById('deselectAllButton').style.display = 'block';
}

// Deselectionne tout les élèments 
function deselectAll() {
    // Sélectionnez toutes les cases à cocher dans la liste
    const checkboxes = document.querySelectorAll('#borrowedItemsList input[type="checkbox"]');

    // Boucle à travers chaque case à cocher et la cocher
    checkboxes.forEach(checkbox => {
        checkbox.checked = false;
    });

    // Masquer le bouton "Deselect All" et afficher le bouton "Select All"
    document.getElementById('selectAllButton').style.display = 'block';
    document.getElementById('deselectAllButton').style.display = 'none';
}

// Cette fonction sera déclenchée lorsque le bouton "Unborrow" est cliqué dans mon panier
function unborrowSelected() {
    // Récupérez l'utilisateur actuellement connecté
    const userString = localStorage.getItem("user");
    const userObject = JSON.parse(userString);
    const username = userObject.username;

    // Select all checked checkboxes in the list
    const checkedCheckboxes = document.querySelectorAll('#borrowedItemsList input[type="checkbox"]:checked');

    if (checkedCheckboxes.length === 0) {
        alert('Please select at least one item to unborrow.');
        return;
    }

    // Get all borrowed items once
    fetch('http://localhost:3000/borrowed-items')
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(items => {
            checkedCheckboxes.forEach(checkbox => {
                // console.log('checkbox',checkbox)
                const userBorrowedItems = items.filter(item => item._id === checkbox.id);
                console.log('userBorrowedItems - ', userBorrowedItems)
                userBorrowedItems.forEach(item => {
                    redirectToUnborrowForm(item.id_item_borrowed);
                    const itemId = item._id;
                    fetch(`http://localhost:3000/borrowed-items/${itemId}`, {
                        method: 'DELETE',
                    })
                        .then(response => {
                            if (!response.ok) {
                                // If the response is not OK, throw an error
                                throw new Error('Network response was not ok');
                            }
                            // Log a success message if the deletion is successful
                            console.log(`Borrowed (2) item with ID ${itemId} has been deleted`);
                            // Ajouter l'opération à l'historique
                            let tmp_date = new Date();
                            const historyItem = {
                                user: username,
                                date: tmp_date,
                                description: `Item unborrowed: ${item.name}`,
                                type: 'unborrow',
                                validated: false,
                                quantity: item.quantity
                            };

                            fetch(`http://localhost:3000/admin/history/add`, {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(historyItem),
                            })
                                .then(response => {
                                    if (!response.ok) {
                                        throw new Error('Network response was not ok');
                                    }
                                    return response.json();
                                })
                                .then(data => {
                                    console.log('Historique créé avec succès :', data);
                                    window.location.reload();
                                })
                                .catch(error => {
                                    console.error('There has been a problem with your fetch operation:2', error);
                                });


                        })
                        .catch(error => {
                            // Log an error message if there's a problem with the fetch operation
                            console.error('There has been a problem with your fetch operation: 1', error);
                        });
                });
                closeMyItemsPopup();
            });
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });

}

// Fonction pour changer le statut de l'objet
function changeItemStatus(itemId, status) {
    fetch(`http://localhost:3000/items/${itemId}`, {
        method: 'PATCH', // ou 'PUT' si votre API le requiert pour les mises à jour
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ status: status }), // Mettre à jour le statut à 'Available'
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log('Item status updated:', data);
            // Ici, vous pouvez actualiser l'UI pour refléter le changement de statut de l'item
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });
}



// Ajoutez cet événement de clic à votre bouton "addQuantity" après avoir récupéré la quantité de l'objet sélectionné
const addQuantityBtn = document.getElementById("addQuantity");
addQuantityBtn.addEventListener("click", () => {
    // Récupérez l'ID de l'objet sélectionné depuis le menu déroulant
    const dropdown = document.getElementById("itemSelect");
    const selectedID = dropdown.value;

    // Récupérez la quantité depuis votre API (assurez-vous de stocker cette valeur dans une variable)
    let maxQuantity; // Variable pour stocker la quantité de l'objet

    fetch(`http://localhost:3000/items/${selectedID}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(itemDetails => {
            maxQuantity = itemDetails.quantity; // Récupération de la quantité depuis l'API
            // alert("Max quantity", maxQuantity); const


            // Récupérez la quantité actuelle depuis votre interface utilisateur
            const quantityInput = document.getElementById("quantity");
            let currentQuantity = parseInt(quantityInput.value, 10);

            // Effectuez une vérification pour ne pas dépasser la limite de quantité de l'objet
            if (currentQuantity < maxQuantity) {
                currentQuantity++;
                quantityInput.value = currentQuantity;
            }
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });
});

// Ajoutez cet événement de clic à votre bouton "removeQuantity"
const removeQuantityBtn = document.getElementById("removeQuantity");
removeQuantityBtn.addEventListener("click", () => {
    // Récupérez la quantité actuelle depuis votre interface utilisateur
    const quantityInput = document.getElementById("quantity");
    let currentQuantity = parseInt(quantityInput.value, 10);

    // Effectuez une vérification pour ne pas descendre en dessous de 1
    if (currentQuantity > 1) {
        currentQuantity--;
        quantityInput.value = currentQuantity;
    }
});


