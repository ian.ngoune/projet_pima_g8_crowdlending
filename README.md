# Projet_PIMA_g8_Crowdlending

# Prerequis
- Telecharger/Installer la version "v18.16.1" de "NodeJs"
- Telecharger/Installer "PostMan Agent" Pour tester les api requests

# Apres avoir cloné/pull le projet
- Dans le repertoire du projet lancer :
    ------------------------------------------------
    1° pour installer les package node : npm install
    2° pour lancer le server : nodemon server.js
    ------------------------------------------------

# CONNECTER VOUS AVEC :
    {
    "username": "User",
    "password": "0000"
    }

# Some expalanations
- Dans le Dossier Models:
    1° itemModel.js : schema and model pour le item de la Bd
    1° userModel.js : schema and model pour le user de la Bd

- Les Models sont utilisés dans server.js

- Dans le fichier server.js : 
    1° Le setup et la connexion a la DB MongoDB
    2° Des Api sont ecirit pour faire des operations ver la BD (login, recuperer de item de la Bd, ...)

- Le fichier main.js :
    Le code pour la page d'authentication et appel ver notre api login

- Mise à jour de la fonction populateDropdown() et displayItemDetails() du fichier script.js pour faire des apis calls pour que le US 1 que j'ai fais fonction avec les data de la DB

# ECRIVEZ VOUS API (WITH Post/Get/Put/Patch/Delete requeqt) SI NECESSAIRE TESTER LES AVEC POSTMAN ET UTILISER LES BIEN


